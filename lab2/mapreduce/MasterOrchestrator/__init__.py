# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df
import simplejson as json


def orchestrator_function(context: df.DurableOrchestrationContext):

    # input_str: str = context.get_input()
    #lines = ["a b", "b c", "b b"] # divide by lines
    input_results = yield context.call_activity("GetInputDataFn")

    tasks = []
    for line_num, line in enumerate(input_results):
        tasks.append(context.call_activity("Mapper", json.dumps((line_num, line))))
    
    map_results = yield context.task_all(tasks)
    # print(map_results)

    # shuffle
    shuffle_results = yield context.call_activity("Shuffler", json.dumps(map_results))

    tasks = []
    for i, (word, cnt_list) in enumerate(shuffle_results):
        tasks.append(context.call_activity("Reducer", json.dumps((word, cnt_list))))

    reduce_results = yield context.task_all(tasks)
    return reduce_results

main = df.Orchestrator.create(orchestrator_function)