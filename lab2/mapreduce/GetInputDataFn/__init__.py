# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import os
from typing import List
from azure.storage.blob import BlobServiceClient


def main(name: str) -> List[str]:
    # Retrieve the connection string for use with the application. The storage
    # connection string is stored in an environment variable on the machine
    # running the application called AZURE_STORAGE_CONNECTION_STRING. If the environment variable is
    # created after the application is launched in a console or with Visual Studio,
    # the shell or application needs to be closed and reloaded to take the
    # environment variable into account.
    connect_str = "DefaultEndpointsProtocol=https;AccountName=durablemapreduce;AccountKey=yI0MbqFXTxH/DfOIuLYzRIE1l0kwywFgd7OpVyhcwvDFv2QLVFhcTpt/r71XI0PbFb/1GPEc4Yk0+AStfHPatQ==;EndpointSuffix=core.windows.net"

    # Create the BlobServiceClient object
    blob_service_client = BlobServiceClient.from_connection_string(connect_str)
    container_client = blob_service_client.get_container_client(container= "mrinput") 

    input_lines = []
    for blob in container_client.list_blobs():
        file_txt = container_client.download_blob(blob.name).readall().decode("utf-8")
        # input_lines.append(map(list, zip(*file_txt.split("\r\n"))))
        input_lines.extend(file_txt.split("\r\n"))

    return input_lines
