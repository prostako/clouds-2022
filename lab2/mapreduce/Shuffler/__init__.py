# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from typing import Tuple, List
import simplejson as json


def main(kvlist: str) -> List[Tuple[str, List[int]]]:
    kvlist = json.loads(kvlist)
    output = {}
    for inner_list in kvlist:
        for key, value in inner_list:
            if key not in output.keys():
                output[key] = [value]
            else:
                output[key].append(value)

    return [(k, output[k]) for k in output.keys()]
