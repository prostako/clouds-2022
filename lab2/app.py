from flask import Flask
import sys, math

N_SUBINTERVALS = [10, 100, 100, 1000, 10**4, 10**5, 10**6]

def num_integr(lower, upper, N):
    dx = (upper-lower)/N
    integr = 0.0
    for i in range(N):
        xip12 = lower + dx*(i + 0.5)
        dI = abs(math.sin(xip12))*dx
        integr += dI
    return integr

app = Flask(__name__)


#@app.route('/numintegrationservice/<float:lower>/<float:upper>', methods=('GET', 'POST'))
@app.route('/numintegrationservice/<lower>/<upper>', methods=('GET', 'POST'))
def index(lower, upper):
    lower, upper = map(float, [lower, upper])
    res_list = [num_integr(lower, upper, N) for N in N_SUBINTERVALS]
    return res_list
