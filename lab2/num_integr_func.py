import logging
import sys, math
import azure.functions as func

N_SUBINTERVALS = [10, 100, 100, 1000, 10**4, 10**5, 10**6]

def num_integr(lower, upper, N):
    dx = (upper-lower)/N
    integr = 0.0
    for i in range(N):
        xip12 = lower + dx*(i + 0.5)
        dI = abs(math.sin(xip12))*dx
        integr += dI
    return integr

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('lower')
    upper = req.params.get('upper')
    if not lower:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('lower')
    if not upper:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            name = req_body.get('upper')

    if lower and upper:
        lower, upper = map(float, [lower, upper])
        res_list = [num_integr(lower, upper, N) for N in N_SUBINTERVALS]
        return func.HttpResponse(f"Here is the results list: {res_list}. This HTTP triggered function executed successfully.")
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass lower and upper in the query string or in the request body for a personalized response.",
             status_code=200
        )
