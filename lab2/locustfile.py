#import time
from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def hello(self):
        self.client.get("/numintegrationservice/0/3.14159")
