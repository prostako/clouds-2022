import sys, math # argparse?

N_SUBINTERVALS = [10, 100, 100, 1000, 10**4, 10**5, 10**6]

def num_integr(lower, upper, N):
    dx = (upper-lower)/N
    integr = 0.0
    for i in range(N):
        xip12 = lower + dx*(i + 0.5)
        dI = abs(math.sin(xip12))*dx
        integr += dI
    return integr

def main(argv):
    lower, upper = map(float, argv)
    #lower, upper = map(float, input().split())
    res_list = [num_integr(lower, upper, N) for N in N_SUBINTERVALS]
    print(res_list)

if __name__ == "__main__":
    main(sys.argv[1:])
